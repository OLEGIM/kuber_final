# Kuber_Final

* Проект robot_shop 

Веб-страница представляет собой одностраничное приложение его ресурсы обслуживаются Nginx, который также выступает в качестве обратного прокси-сервера для внутренних микросервисов.

MongoDB используется в качестве хранилища данных для каталога товаров и зарегистрированных пользователей.
MySQL используется для поиска информации о доставке.
Redis используется для хранения активных корзин покупок.
Конвейер заказа обрабатывается через RabbitMQ


## Автоматическая установка
Не готово

## Ручная установка

## Установка кластера GKE используя terraform

### [Установка кластера] (https://gitlab.com/OLEGIM/tf-gke/)

### Настроить kubectl на работу с контекстом

```bash
gcloud container clusters get-credentials cluster --zone europe-west2-a --project final-275021
```


## Запуск образов приложения

Для настройки своего репозитория в файле .env указываем имя образа и его тег

* docker-compose build

* docker-compose push

## Развертывание приложения

```shell
cd K8s/helm
##kubectl create ns service
##kubectl create ns robot-shop
kubectl apply -f namespaces/
kubectl apply -f cluster-rbac/
##helm upgrade --install robot-shop --namespace robot-shop .
helm upgrade --install flux fluxcd/flux -f flux.values.yaml --namespace robot-shop
helm upgrade --install helm-operator fluxcd/helm-operator -f helm-operator.values.yaml --namespace robot-shop

istioctl manifest apply --set profile=demo
kubectl label namespace robot-shop istio-injection=enabled
```

Установим CRD для корректной работы

```bash
kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/release-0.35/example/prometheus-operator-crd/monitoring.coreos.com_alertmanagers.yaml --validate=false && kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/release-0.35/example/prometheus-operator-crd/monitoring.coreos.com_podmonitors.yaml --validate=false && kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/release-0.35/example/prometheus-operator-crd/monitoring.coreos.com_prometheuses.yaml --validate=false && kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/release-0.35/example/prometheus-operator-crd/monitoring.coreos.com_prometheusrules.yaml --validate=false && kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/release-0.35/example/prometheus-operator-crd/monitoring.coreos.com_servicemonitors.yaml --validate=false
```

Применим deployment с nginx, service и servicemonitor

```bash
kubectl apply -f deployment.yaml --validate && kubectl apply -f service.yaml --validate && kubectl apply -f servicemonitor.yaml --validate
```

Установка helm chart`a prometheus-operator

```bash
helm upgrade --install prometheus stable/prometheus-operator --namespace=service -f values.yaml